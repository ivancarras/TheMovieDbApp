package com.wembleystudios.themoviedbapp.domain.model

import java.util.*

data class Movie(
    val id: Int,
    val title: String? = null,
    val overview: String? = null,
    val image: String? = null,
    val releaseDate: Date? = null
)