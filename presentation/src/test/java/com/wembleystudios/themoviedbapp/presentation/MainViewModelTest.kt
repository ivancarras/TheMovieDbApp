package com.wembleystudios.themoviedbapp.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wembleystudios.themoviedbapp.domain.model.Movie
import com.wembleystudios.themoviedbapp.domain.model.MoviesPage
import com.wembleystudios.themoviedbapp.domain.usecase.GetPopularMoviesUseCase
import com.wembleystudios.themoviedbapp.domain.usecase.SearchMoviesUseCase
import com.wembleystudios.themoviedbapp.presentation.mapper.MoviePresentationMapper
import com.wembleystudios.themoviedbapp.presentation.mapper.MoviesPagePresentationMapper
import com.wembleystudios.themoviedbapp.presentation.viewmodel.MainViewModel
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito

class MainViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase = mock()
    private val searchMoviesUseCase: SearchMoviesUseCase = mock()
    private lateinit var mainViewModel: MainViewModel

    private fun viewModelSetUp() {
        //We need do it of this way because of the calls did in the init()
        mainViewModel = MainViewModel(
            getPopularMoviesUseCase = getPopularMoviesUseCase,
            moviesPageMapper = MoviesPagePresentationMapper(moviePresentationMapper = MoviePresentationMapper()),
            observeOnScheduler = Schedulers.trampoline(),
            searchMoviesUseCase = searchMoviesUseCase,
            subscribeOnScheduler = Schedulers.trampoline()
        )
    }

    @Test
    fun `OnSearch film success`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(Mockito.anyInt())).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = false,
                    results = listOf(Movie(0))
                )
            )
        )

        Mockito.`when`(searchMoviesUseCase.searchMovies(Mockito.anyString(), Mockito.anyInt()))
            .thenReturn(
                Single.just(
                    MoviesPage(
                        page = 0,
                        hasMoreResults = false,
                        results = listOf(Movie(1))
                    )
                )
            )
        viewModelSetUp()

        //When
        val stateLiveData = mainViewModel.stateLiveData.testObserver()
        mainViewModel.onSearch("Movie X")

        //Then
        val loadingState = stateLiveData.observedValues[stateLiveData.observedValues.lastIndex - 1]
        assert(!loadingState.isError && loadingState.isLoading)

        val successState = stateLiveData.observedValues.last()
        assert(!successState.isLoading && !successState.isError && successState.movies.isNotEmpty())
    }

    @Test
    fun `OnLoad more films success`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(Mockito.anyInt())).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = true,
                    results = listOf(Movie(0))
                )
            )
        )

        viewModelSetUp()

        //When
        val stateLiveData = mainViewModel.stateLiveData.testObserver()
        mainViewModel.loadMore()
        //Then
        val loadingState = stateLiveData.observedValues[stateLiveData.observedValues.lastIndex - 1]
        assert(!loadingState.isError && loadingState.isLoading)

        val successState = stateLiveData.observedValues.last()
        assert(!successState.isLoading && !successState.isError && successState.movies.isNotEmpty())

    }

    @Test
    fun `GetPopulars movies success`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(Mockito.anyInt())).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = false,
                    results = listOf(Movie(0))
                )
            )
        )

        //When
        viewModelSetUp()
        val stateLiveData = mainViewModel.stateLiveData.testObserver()

        //Then
        val successState = stateLiveData.observedValues.last()
        assert(!successState.isLoading && !successState.isError && successState.movies.isNotEmpty())
    }

    @Test
    fun `OnSearch film error`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(Mockito.anyInt())).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = true,
                    results = listOf(Movie(0))
                )
            )
        )

        Mockito.`when`(searchMoviesUseCase.searchMovies(Mockito.anyString(), Mockito.anyInt()))
            .thenReturn(
                Single.error(
                    Throwable("Unit test error")
                )
            )
        viewModelSetUp()

        //When
        val stateLiveData = mainViewModel.stateLiveData.testObserver()
        mainViewModel.onSearch("Movie X")

        //Then
        val loadingState = stateLiveData.observedValues[stateLiveData.observedValues.lastIndex - 1]
        assert(!loadingState.isError && loadingState.isLoading)

        val errorState = stateLiveData.observedValues.last()
        assert(!errorState.isLoading && errorState.isError)
    }

    @Test
    fun `OnLoad more films error`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(1)).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = true,
                    results = listOf(Movie(0))
                )
            )
        )

        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(2)).thenReturn(
            Single.error(
                Throwable("Unit test error")
            )
        )

        viewModelSetUp()

        //When
        val stateLiveData = mainViewModel.stateLiveData.testObserver()
        mainViewModel.loadMore()
        //Then
        val loadingState = stateLiveData.observedValues[stateLiveData.observedValues.lastIndex - 1]
        assert(!loadingState.isError && loadingState.isLoading)

        val errorState = stateLiveData.observedValues.last()
        assert(!errorState.isLoading && errorState.isError)

    }

    @Test
    fun `GetPopulars movies error`() {
        //Given
        Mockito.`when`(getPopularMoviesUseCase.getPopularMovies(0)).thenReturn(
            Single.just(
                MoviesPage(
                    page = 0,
                    hasMoreResults = true,
                    results = listOf(Movie(0))
                )
            )
        )

        //When
        viewModelSetUp()
        val stateLiveData = mainViewModel.stateLiveData.testObserver()

        //Then
        val errorState = stateLiveData.observedValues.last()
        assert(!errorState.isLoading && errorState.isError && errorState.movies.isEmpty())
    }

}