package com.wembleystudios.themoviedbapp.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer


class TestObserver<T> : Observer<T> {
    val observedValues = mutableListOf<T>()
    override fun onChanged(t: T) {
        observedValues.add(t)
    }
}

fun <T> LiveData<T>.testObserver(): TestObserver<T> =
    TestObserver<T>().apply {
        observeForever(this)
    }
